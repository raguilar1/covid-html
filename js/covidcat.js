var url = "https://api.covid19api.com/live/country/peru/status/confirmed";
var url2 = "https://api.covid19api.com/summary"; 
var nueva_url = "https://covid-ams.operanewsapp.com/vmact/activities/covid19/todaydata?country=es&language=es";
var spain = "https://api.covid19api.com/live/country/Spain/status/confirmed"
var url_actual = "https://corona-api.com/countries/es";

function Datos_generales(url) {

  let confirmados = document.getElementById('confirmed');
  let recuperados = document.getElementById('recovered');
  let muertos = document.getElementById('deaths');

  fetch(url)
    .then(response => response.json())
    .then(data => {

      console.log(data['data']);

      for (const datos of data['data']) {

        console.log(datos);
          confirmados.innerHTML = datos.confirm_total;
          recuperados.innerHTML = datos.cure_total;
          muertos.innerHTML = datos.died_total;
      }
    });
};


function Datos_piechart(url) {
  fetch(url)
    .then(response => response.json())
    .then(data => {
      for (const datos of data['data']) {

        //console.log(datos);
          var pdata = [
            {
              value: datos.confirm_total,
              color: "#FDB45C",
              highlight: "#FDB45C",
              label: "Confirmats"
            },
            {
              value: datos.cure_total,
              color: "#009688",
              highlight: "#009688",
              label: "Recuperats"
            },
            {
              value: datos.died_total,
              color: "#F7464A",
              highlight: "#FF5A5E",
              label: "Morts"
            }
          ]
        
      }

      var ctxp = $("#pieChartDemo").get(0).getContext("2d");
      var pieChart = new Chart(ctxp).Pie(pdata);
    });
}
function Datos_linechart(url) {

  fetch(url)
    .then(response => response.json())
    .then(({ data }) => {

      const timeline = data.timeline;

      const dates = [];
      const confirmados = [];
      const recuperados = [];
      const muertes = [];

      timeline.forEach(element => {
        dates.push(element.date);
      });
      

      timeline.forEach(element => {
        confirmados.push(element.confirmed);
      }); 

      timeline.forEach(element => {
        recuperados.push(element.recovered);
      });

      timeline.forEach(element => {
        muertes.push(element.deaths);
      });

      var data = {
        labels: dates.sort(),
        datasets: [
          {
            label: "Casos confirmats",
            fillColor: "rgba(220,220,220,0.2)",
            strokeColor: "rgba(255, 209, 51)",
            pointColor: "rgba(255, 209, 51)",
            pointStrokeColor: "#fff",
            pointHighlightFill: "#fff",
            pointHighlightStroke: "rgba(255, 209, 51)",
            data: confirmados.sort((a, b) => a - b)
          },
          {
            label: "Casos Recuperats",
            fillColor: "rgba(220,220,220,0.2)",
            strokeColor: "rgba(28, 206, 66 )",
            pointColor: "rgba(28, 206, 66 )",
            pointStrokeColor: "#fff",
            pointHighlightFill: "#fff",
            pointHighlightStroke: "rgba(28, 206, 66 )",
            data: recuperados.sort((a, b) => a - b)
          },
          {
            label: "Morts",
            fillColor: "rgba(220,220,220,0.2)",
            strokeColor: "rgba(255, 51, 88 )",
            pointColor: "rgba(255, 51, 88 )",
            pointStrokeColor: "#fff",
            pointHighlightFill: "#fff",
            pointHighlightStroke: "rgba(255, 51, 88 )",
            data: muertes.sort((a, b) => a - b)
          }

        ],
        options: {
          responsive: true,
          title: {
            display: true,
            text: 'Chart.js Line Chart'
          }
           
          }
        
      };
      var ctxl = $("#lineChartDemo").get(0).getContext("2d");
      var lineChart = new Chart(ctxl).Line(data);

    })
    .catch((error) => console.log(error))
}

function Summary(url) {
  let confirmados = document.getElementById('nuevos-confirmados');
  let recuperados = document.getElementById('nuevos-recuperados');
  let muertes = document.getElementById('nuevas-muertes');

  fetch(url)
    .then(response => response.json())
    .then(data => {
        let datos = data['Countries'];
        let Peru = datos[208];
        
        console.log(Peru);
        confirmados.innerHTML = Peru.NewConfirmed;
        recuperados.innerHTML = Peru.NewRecovered;
        muertes.innerHTML = Peru.NewDeaths;
    });
}

Datos_generales(nueva_url);
Datos_piechart(nueva_url);
Summary(url2);
Datos_linechart(url_actual);





