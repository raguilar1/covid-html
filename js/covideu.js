var url = "https://api.covid19api.com/live/country/peru/status/confirmed";
var url2 = "https://api.covid19api.com/summary"; 
var nueva_url = "https://covid-ams.operanewsapp.com/vmact/activities/covid19/todaydata?country=es&language=es";
var spain = "https://api.covid19api.com/live/country/Spain/status/confirmed"
var url_actual = "https://corona-api.com/countries/es";
var europe = "https://coronavirus-19-api.herokuapp.com/countries";

function Datos_generales(url) {

  let confirmados = document.getElementById('confirmed');
  let recuperados = document.getElementById('recovered');
  let muertos = document.getElementById('deaths');

  fetch(europe)
    .then(response => response.json())
    .then(data => {

        console.log(data[1])
          confirmados.innerHTML = data[1].cases;
          recuperados.innerHTML = data[1].recovered;
          muertos.innerHTML = data[1].deaths;
      
    });
};


function Datos_piechart(europe) {
fetch(europe)
    .then(response => response.json())
    .then(data => {

        console.log(data[1]) 

        //console.log(datos);
          var pdata = [
            {
              value: data[1].cases,
              color: "#FDB45C",
              highlight: "#FDB45C",
              label: "Confirmados"
            },
            {
              value: data[1].recovered,
              color: "#009688",
              highlight: "#009688",
              label: "Recuperados"
            },
            {
              value: data[1].deaths,
              color: "#F7464A",
              highlight: "#FF5A5E",
              label: "Muertes"
            }
          ]
        
      

      var ctxp = $("#pieChartDemo").get(0).getContext("2d");
      var pieChart = new Chart(ctxp).Pie(pdata);
    });
}

function Summary(url) {
  let confirmados = document.getElementById('nuevos-confirmados');
  let recuperados = document.getElementById('nuevos-recuperados');
  let muertes = document.getElementById('nuevas-muertes');

  fetch(url)
    .then(response => response.json())
    .then(data => {
        let datos = data[1];
        
        console.log(datos);
        confirmados.innerHTML = datos.todayCases;
        recuperados.innerHTML = datos.NewRecovered;
        muertes.innerHTML = datos.todayDeaths;
    });
}

Datos_generales(europe);
Datos_piechart(europe);
Summary(europe);






