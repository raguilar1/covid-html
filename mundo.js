var url = "https://api.covid19api.com/live/country/peru/status/confirmed";
var url2 = "https://api.covid19api.com/summary"; 
var nueva_url = "https://covid-ams.operanewsapp.com/vmact/activities/covid19/todaydata?country=it&language=es";
var spain = "https://api.covid19api.com/live/country/Spain/status/confirmed"


function Datos_generales(url) {

  let confirmados = document.getElementById('confirmed');
  let recuperados = document.getElementById('recovered');
  let muertos = document.getElementById('deaths');

  fetch(url)
    .then(response => response.json())
    .then(data => {

  let datos = data['Global'];
        let Peru = datos;
 console.log(Peru);
          confirmados.innerHTML = datos.TotalConfirmed;
          recuperados.innerHTML = datos.TotalRecovered;
          muertos.innerHTML = datos.TotalDeaths;
    });
};


function Datos_piechart(url) {
  fetch(url)
    .then(response => response.json())
    .then(data => {
 let datos = data['Global'];
        let Peru = datos;
 console.log(Peru);
          var pdata = [
            {
              value: datos.TotalConfirmed,
              color: "#FDB45C",
              highlight: "#FDB45C",
              label: "Confirmed"
              
            },
            {
              value: datos.TotalRecovered,
              color: "#009688",
              highlight: "#009688",
              label: "Recovered"
            },
            {
              value: datos.TotalDeaths,
              color: "#F7464A",
              highlight: "#FF5A5E",
              label: "Deaths"
            }
          ]
        
      

      var ctxp = $("#pieChartDemo").get(0).getContext("2d");
      var pieChart = new Chart(ctxp).Pie(pdata);
    });
}

function Summary(url) {
  let confirmados = document.getElementById('nuevos-confirmados');
  let recuperados = document.getElementById('nuevos-recuperados');
  let muertes = document.getElementById('nuevas-muertes');

  fetch(url)
    .then(response => response.json())
    .then(data => {
        let datos = data['Global'];
        let Peru = datos;
        
        console.log(Peru);
        confirmados.innerHTML = Peru.NewConfirmed;
        recuperados.innerHTML = Peru.NewRecovered;
        muertes.innerHTML = Peru.NewDeaths;
    });
}

Datos_generales(url2);
Datos_piechart(url2);
Summary(url2);






